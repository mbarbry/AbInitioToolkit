Ab Initio Toolkit
=================

Using ab initio program (Siesta-Pyscf) without any installation using containers


Dependencies
------------
 
Installing [Singularity](http://singularity.lbl.gov/) on Linux

* from the repository

Get repository and update

    sudo wget -O- http://neuro.debian.net/lists/xenial.us-ca.full | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
    sudo apt-key adv --recv-keys --keyserver hkp://pool.sks-keyservers.net:80 0xA5D32F012649A5A9
    sudo apt-get update
    sudo apt-get install -y singularity-container

* manually

dependencies

    sudo apt-get update
    sudo apt-get install python dh-autoreconf build-essential

manual installation of singularity

    git clone https://github.com/singularityware/singularity.git
    cd singularity/
    ./autogen.sh
    ./configure --prefix=/usr/local
    make
    sudo make install

Build the image
--------------------

This step is necessary only if you do not have the image PySCF-NAO-Siesta.img
to build the image (take around 10 min with a good internet connection)

    singularity build PySCF-NAO-Siesta.img buildPySCF-NAO-Siesta 

Use singularity to run the container
------------------------------------

to run the executable, first create a tmp folder for the container

    mkdir $HOME/tmpSingularity

run the image

    singularity run --bind $HOME/tmpSingularity:/dataSingularity PySCF-NAO-Siesta.img

The last command will run a Jupyter notebook, you can open it by clicking on the link
and then enjoy running siesta and pyscf!!

You can also run a traditional shell if you find the notebook uncomfortable

    singularity shell --bind $HOME/tmpSingularity:/dataSingularity PySCF-NAO-Siesta.img


Nvidia support
--------------

To run a container with Nvidia GPU support, use the `--nv` option

    singularity shell --nv PySCF-NAO-Siesta.img

