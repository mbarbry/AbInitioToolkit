
Easy Ab initio calculation with ASE-Siesta-Pyscf
================================================

No installation necessary, just download a ready to go container for any system, or run it into the cloud
---------------------------------------------------------------------------------------------------------

We first import the necessary libraries and define the system using ASE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    # import libraries and set up the molecule geometry
    
    from ase.units import Ry, eV, Ha
    from ase.calculators.siesta import Siesta
    from ase import Atoms
    import numpy as np
    import matplotlib.pyplot as plt
    
    H2O = Atoms('H2O', positions = [[-0.757,  0.586,  0.000],
                                    [0.757,  0.586,  0.000],
                                    [0.0, 0.0, 0.0]],
                cell=[20, 20, 20])
    
    # visualization of the particle
    from ase.visualize import view
    view(H2O, viewer='x3d')




.. raw:: html

    <html>
    
     <head>
    
      <title>ASE atomic visualization</title>
    
      <link rel="stylesheet" type="text/css"
    
       href="https://www.x3dom.org/x3dom/release/x3dom.css">
    
      </link>
    
      <script type="text/javascript"
    
       src="https://www.x3dom.org/x3dom/release/x3dom.js">
    
      </script>
    
     </head>
    
     <body>
    
      <X3D style="margin:0; padding:0; width:100%; height:100%; border:none;">
    
       <Scene>
    
        <Transform translation="-0.76 0.59 0.00">
    
         <Shape>
    
          <Appearance>
    
           <Material diffuseColor="1.000 1.000 1.000" specularColor="0.5 0.5 0.5">
    
           </Material>
    
          </Appearance>
    
          <Sphere radius="0.31">
    
          </Sphere>
    
         </Shape>
    
        </Transform>
    
        <Transform translation="0.76 0.59 0.00">
    
         <Shape>
    
          <Appearance>
    
           <Material diffuseColor="1.000 1.000 1.000" specularColor="0.5 0.5 0.5">
    
           </Material>
    
          </Appearance>
    
          <Sphere radius="0.31">
    
          </Sphere>
    
         </Shape>
    
        </Transform>
    
        <Transform translation="0.00 0.00 0.00">
    
         <Shape>
    
          <Appearance>
    
           <Material diffuseColor="1.000 0.051 0.051" specularColor="0.5 0.5 0.5">
    
           </Material>
    
          </Appearance>
    
          <Sphere radius="0.66">
    
          </Sphere>
    
         </Shape>
    
        </Transform>
    
       </Scene>
    
      </X3D>
    
     </body>
    
    </html>
    




We can then run the DFT calculation using Siesta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    # enter siesta input and run siesta
    siesta = Siesta(
        mesh_cutoff=150 * Ry,
        basis_set='DZP',
        pseudo_qualifier='lda',
        energy_shift=(10 * 10**-3) * eV,
        fdf_arguments={
            'SCFMustConverge': False,
            'COOP.Write': True,
            'WriteDenchar': True,
            'PAO.BasisType': 'split',
            'DM.Tolerance': 1e-4,
            'DM.MixingWeight': 0.01,
            'MaxSCFIterations': 300,
            'DM.NumberPulay': 4,
            'XML.Write': True})
    
    H2O.set_calculator(siesta)
    e = H2O.get_potential_energy()

The TDDFT calculations with PySCF-NAO
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    # compute polarizability using pyscf-nao
    
    siesta.pyscf_tddft(label="siesta", jcutoff=7, iter_broadening=0.15/Ha,
            xc_code='LDA,PZ', tol_loc=1e-6, tol_biloc=1e-7, freq = np.arange(0.0, 15.0, 0.05))


.. code:: python

    # plot polarizability with matplotlib
    %matplotlib inline
    
    fig = plt.figure(1)
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.plot(siesta.results["freq range"], siesta.results["polarizability nonin"][:, 0, 0].imag)
    ax2.plot(siesta.results["freq range"], siesta.results["polarizability inter"][:, 0, 0].imag)
    
    ax1.set_xlabel(r"$\omega$ (eV)")
    ax2.set_xlabel(r"$\omega$ (eV)")
    
    ax1.set_ylabel(r"Im($P_{xx}$) (au)")
    ax2.set_ylabel(r"Im($P_{xx}$) (au)")
    
    ax1.set_title(r"Non interacting")
    ax2.set_title(r"Interacting")
    
    fig.tight_layout()



.. image:: output_7_0.png
