Installation instructions
=========================

To use a container image, you first need to download and install
Singularity_.  Once you have it installed, create a new directory
in which you will be able to access the data contains in the container.::

  $ mkdir $HOME/tmpSingularity 

You just then need to use the following command to run the Jupyter notebook_
interface.::

  $ singularity run --bind $HOME/tmpSingularity:/dataSingularity image-name.simg

you can then connect to the notebook by clicking on the link appearing in the terminal.
If you prefer to use a shell interface, just run::

  $ singularity shell --bind $HOME/tmpSingularity:/dataSingularity image-name.simg


.. _Singularity: http://singularity.lbl.gov/install-linux
.. _notebook: https://jupyter.org 
