.. Ab-initio Toolkit documentation master file, created by
   sphinx-quickstart on Mon Mar 12 11:11:33 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ab-initio Toolkit's documentation!
=============================================

.. image:: _images/logo.png
   :width: 100%

Ab-initio Toolkit is a collection of containers_ which contains all the necessary tools to
easily run Ab-initio calculations on Linux, Mac or Windows. It uses the powerful
Singularity_ program to create the container image. All that you need to do is to install
Singularity on your computer, download the image that you want and run your calculations.
The image can work on many platforms, like your Desktop, a powerful cluster or cloud
services such as AWS_ or Google-Cloud_.


To ask for a container, please write to
marc.barbry@mailoo.org, koval.peter@gmail.com or daniel.sanchez@ehu.eus

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation.rst
   examples.rst

.. _containers: https://en.wikipedia.org/wiki/Operating-system-level_virtualization
.. _Singularity: http://singularity.lbl.gov/
.. _AWS: https://aws.amazon.com/
.. _Google-Cloud: https://cloud.google.com/


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
