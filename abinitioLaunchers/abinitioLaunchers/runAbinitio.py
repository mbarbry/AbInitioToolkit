from __future__ import division
import argparse
import os
import numpy as np
import abinitioLaunchers.utilities as utils

import ase.io as io
from ase.units import Ry, eV, Ha

def launch_DFT_gto(fname, basis="ccpvdz", spin=0):
    """
    Launch DFT calculation with PySCF gto
    """

    from pyscf import gto, scf

    atoms = io.read(fname)

    pyscf_atoms = ""

    for atom in atoms:
        pyscf_atoms += " {0} {1:.3f}, {2:.3f}, {3:.3f};".format(atom.symbol,
                                                                atom.position[0],
                                                                atom.position[1],
                                                                atom.position[2])

    mol = gto.M(atom=pyscf_atoms, basis=basis, spin=spin)
    gto_mf = scf.UHF(mol)
    gto_mf.kernel()

def launch_DFT_siesta(fname, params):
    """
    launch DFT Calculations with Siesta
    """

    from ase.calculators.siesta import Siesta

    atoms = io.read(fname)

    siesta = Siesta(**params)
    atoms.set_calculator(siesta)
    e = atoms.get_potential_energy()
    print("DFT potential energy: ", e)

def launch_GW_siesta(params):

    from pynao import gw_iter

    if "dtype" in params.keys():
        params["dtype"] = utils.get_dtype(params["dtype"])
    else:
        params["dtype"] = np.float64


    gw = gw_iter(cd=os.getcwd(), **params)
    gw.kernel_gw_iter()
    gw.report()

def launch_TDDFT_siesta(params):
    """
    Main parameters:
        label (string)
        jcutoff (int)
        iter_broadening (float): broadening for the spectrum, in Hartree
        xc_code (string)
        tol_loc (float)
        tol_biloc (float)
        freq (np.array): frequencies at which to calculate the spectra in eV
    """

    from pynao import tddft_iter

    params["iter_broadening"] = utils.convert2units(params["iter_broadening"][0],
                                                    params["iter_broadening"][1], "Ha")
    freq = utils.convert2units(params["frequency"][0], params["frequency"][1], "Ha")
    del params["frequency"]

    if "dtype" in params.keys():
        params["dtype"] = utils.get_dtype(params["dtype"])
    else:
        params["dtype"] = np.float64


    td = tddft_iter(**params)
    p0mat = td.comp_polariz_nonin_Edir(freq, Eext=params["Edir"])
    pmat = td.comp_polariz_inter_Edir(freq, Eext=params["Edir"])

    np.save("frequencies.npy", freq)
    np.save("polarizability_nonin.npy", p0mat)
    np.save("polarizability_inter.npy", pmat)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('fname', type=str, help='input file name')

    args_par = parser.parse_args()
    geo, methods = utils.convertYaml2calculator(args_par.fname)

    for method in methods:
        if method["name"] == "DFT":
            if method["calculator"] == "Siesta":
                launch_DFT_siesta(geo, method["parameters"])
            else:
                raise ValueError("unknow calculator {}".format(method["calculator"]))

        elif method["name"] == "GW":
            if method["calculator"] == "Siesta":
                launch_GW_siesta(method["parameters"])
            else:
                raise ValueError("unknow calculator {}".format(method["calculator"]))

        elif method["name"] == "TDDFT":
            if method["calculator"] == "Siesta":
                launch_TDDFT_siesta(method["parameters"])
            else:
                raise ValueError("unknow calculator {}".format(method["calculator"]))

        else:
            raise ValueError("unknow method {}".format(method["name"]))
