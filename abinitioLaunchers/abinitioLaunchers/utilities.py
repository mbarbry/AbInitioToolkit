from __future__ import division
import yaml
import numpy as np

from ase.units import Ry, eV, Ha

def get_dtype(dtype):

    if dtype == "float":
        return np.float32
    elif dtype == "double":
        return np.float34
    else:
        raise ValueError("unknow dtype {}".format(dtype))

def convert2units(val, inp_un, out_un):
    """
    This is handle way too few cases ...
    for the moment, only energy conversion with input in eV with ase unit
    definition
    """

    import ase.units as un

    unit_dico = un.create_units("2014")

    if inp_un == out_un:
        return val
    else:
        return val/unit_dico[out_un]

def gather_ranges(ranges):
    """
    Gather range definition into a single numpy array
    """

    out = np.array([])

    for typ, ran in ranges.items():

        if typ == "arange":
            arr = np.arange(ran["begin"], ran["end"], ran["step"])
        elif typ == "list":
            arr = np.array(ran)
        elif typ == "linspace":
            arr = np.linspace(ran["begin"], ran["end"], ran["step"])
        else:
            raise ValueError("unknow range type {}".format(typ))

        out = np.hstack((out, arr))

    return out

def calculators_key2val(key, value):
    """
    Convert yaml parameter to actual parameters for calculators
    """

    # Siesta arguments within fdf_arguments need a special care
    if key == "fdf_arguments":
        args = {}
        for key_fdf, value_fdf in value.items():

            if isinstance(value_fdf, dict):
                if "unit" not in value_fdf.keys():
                    raise ValueError("dict params not implemented")

                args[key_fdf] = (value_fdf["value"], value_fdf["unit"])

            elif isinstance(value_fdf, list):
                raise ValueError("list params not implemented")

            else:
                args[key_fdf] = value_fdf

        return args

    else:
        if isinstance(value, dict):
            if "ranges" in value.keys():
                return (gather_ranges(value["ranges"]), value["unit"])

            elif "unit" in value.keys():
                return (value["value"], value["unit"])

            else:
                return value

        else:
            return value

def convertYaml2calculator(fname):
    """
    Convert Yaml inputs to calculator arguments
    """

    with open(fname, "r") as fl:
        params = yaml.load(fl.read(), Loader=yaml.SafeLoader)

    version = params["version"]
    geo = params["geometry"]
    methods = []
    
    for method_params in params["methods"]:

        method = {}
        method["name"] = method_params["name"]
        calc_params = {}
        calculator = method_params["calculator"]
        yamlparams = method_params["parameters"]
        if method["name"] in ["DFT", "GW", "TDDFT"]:

            if calculator == "Siesta":
                for key, value in yamlparams.items():
                    calc_params[key] = calculators_key2val(key, value)

            else:
                raise ValueError("unknow calculator {}".format(calculator))
        
        else:
            raise ValueError("unknow method {}".format(method["name"]))

        method["calculator"] = calculator
        method["parameters"] = calc_params

        methods.append(method)

    return geo, methods
