from __future__ import division
import os
import numpy as np
import argparse
import yaml
import subprocess

def split(ani, params_fname):

    with open(ani, "r") as fl:
        lines = fl.readlines()

    Natoms = int(lines[0].split()[0])
    Nlines = len(lines)
    linesPerAtoms = Natoms + 2

    Nmols = int(Nlines/linesPerAtoms)
    parameters = []
    for fname in params_fname:
        with open(fname, "r") as fl:
            parameters.append(yaml.load(fl.read(), Loader=yaml.SafeLoader))

    istart = 0
    for imol in range(Nmols):

        folder = "mol{}".format(imol)

        os.mkdir(folder)
        for params, fname in zip(parameters, params_fname):
            params["geometry"] = "mol{}.xyz".format(imol)
            with open("{}/{}".format(folder, fname), "w") as fl:
                yaml.dump(params, fl)

        fname = "{}/mol{}.xyz".format(folder, imol)
        with open(fname, "w") as fl:
            for iline in range(imol*linesPerAtoms, (imol+1)*linesPerAtoms):
                fl.write(lines[iline])

        os.chdir(folder)
        for fname in params_fname:
            cmd = "runAbinitio {}".format(fname)
            print(cmd)
            subprocess.call(cmd, shell=True)

        os.chdir("..")

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('fname', type=str, help='input file name')
    parser.add_argument('--params', type=str, nargs="+",
                        help='parameters file name')

    args_par = parser.parse_args()

    split(args_par.fname, args_par.params)
