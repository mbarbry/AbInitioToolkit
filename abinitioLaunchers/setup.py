import os
import sys

import sysconfig
from setuptools import setup, find_packages
from distutils.command.build_py import build_py as _build_py
from setuptools.command.install import install
from setuptools.command.build_ext import build_ext
from abinitioLaunchers import __version__

CLASSIFIERS = [
'Development Status :: Development',
'Intended Audience :: Developers',
'License :: MIT',
'Programming Language :: Python',
'Programming Language :: Python :: 3',
'Topic :: Software Development',
'Topic :: Scientific/Engineering',
'Operating System :: Linux',
]

NAME             = 'Abinitio toolkit'
MAINTAINER       = 'Simbeyond'
MAINTAINER_EMAIL = 'marc.barbry@mailoo.org'
DESCRIPTION      = 'Abinitio toolkit'
LICENSE          = 'MIT'
AUTHOR           = 'Marc Barbry'
AUTHOR_EMAIL     = 'marc.barbry@mailoo.org'
PLATFORMS        = ['Linux']
requirements = ["numpy", "ase"]
pyth_version = sys.version.split()[0]

if int(pyth_version.split(".")[0]) < 3:
    raise ValueError("pydragonfly requires python3")

setup(
    name=NAME,
    version=__version__,
    description=DESCRIPTION,
    license=LICENSE,
    classifiers=CLASSIFIERS,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    platforms=PLATFORMS,
    packages=find_packages(),
    install_requires=requirements,
    extras_require={
        'YAML':  ["pyyaml>=3.13"],
        },
    python_requires='>=3.5',
    entry_points={"console_scripts": [
        "runAbinitio=abinitioLaunchers.runAbinitio:main",
        "runMulitpleGeo=abinitioLaunchers.runMulitpleGeo:main"]}
)
