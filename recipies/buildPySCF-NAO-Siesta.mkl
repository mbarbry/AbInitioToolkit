Bootstrap: docker
From: ubuntu:18.04

%runscript
  exec /opt/conda/bin/python "$@"

%apprun notebook
  exec /opt/conda/bin/jupyter notebook --notebook-dir=/dataSingularity

%help
  try running with run script_name or --app notebook
  To change the number of thread to use inside python use
  import os
  os.environ["NUMTHREADS"] = "number threads"



%environment
  export PATH="/opt/conda/bin:$PATH"              # Python anaconda
  export XDG_RUNTIME_DIR=$HOME                    # to use jupyter notebook
  export PATH="/opt/siesta-4.1-b4/Obj:$PATH"      # Siesta
  export NUMTHREADS=1
  export ASE_SIESTA_COMMAND="siesta < PREFIX.fdf > PREFIX.out"
  #export SIESTA_COMMAND='siesta < ./%s > ./%s'
  export ASE_PATH="/opt/ase"                      # ASE
  export PYTHONPATH=$ASE_PATH:$PYTHONPATH
  export PATH=$ASE_PATH/tools:$PATH
  export PATH=$ASE_PATH/bin:$PATH
  export SIESTA_SCRIPT=$ASE_PATH/siesta/run_siesta.py
  export PYTHONPATH=/opt/pyscf:$PYTHONPATH                                      # pyscf
  export LD_LIBRARY_PATH=/opt/pyscf/pyscf/lib/deps/lib:${LD_LIBRARY_PATH}
  export LD_LIBRARY_PATH=/opt/conda/lib/mkl/lib:${LD_LIBRARY_PATH}
  export SIESTA_PP_PATH=/opt/pyscf/pyscf/nao/examples/psf/
# pyscf needs the following variable to be able to use multithreading with BLAS
  export MKL_NUM_THREADS="$(nproc --all)"

%labels
  AUTHOR marc.barbry@mailoo.org

%post
  echo "Hello from inside the container"
  echo "The post section is where you can install, and configure your container."
  apt-get update && apt-get -y install git wget curl htop gcc gfortran vim build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev
  
  wget https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh -P /opt
  /bin/bash /opt/Anaconda3-2019.10-Linux-x86_64.sh -b -p /opt/conda
  rm /opt/Anaconda3-2019.10-Linux-x86_64.sh && mkdir -p /opt/conda/lib/mkl/lib && ln -s /opt/conda/lib/libmkl* /opt/conda/lib/mkl/lib/
  export PATH="/opt/conda/bin:$PATH"
  conda update conda && /opt/conda/bin/conda install plotly

  cd /opt && git clone https://gitlab.com/mbarbry/AbInitioToolkit.git
  
  export CC=gcc && export FC=gfortran && export CXX=g++
  cd /opt && wget https://launchpad.net/siesta/4.1/4.1-b4/+download/siesta-4.1-b4.tar.gz && tar -xzf siesta-4.1-b4.tar.gz
  cd siesta-4.1-b4/Obj && sh ../Src/obj_setup.sh && cp /opt/AbInitioToolkit/siesta-arc.make/arch.make.mkl.openmp arch.make
  make -j 4
  
  cd /opt && git clone https://gitlab.com/ase/ase.git && git clone https://github.com/cfm-mpc/pyscf.git
  cd ase && python setup.py install && cd /opt

  cd pyscf && git fetch && git checkout nao2 && cd pyscf/lib
  cp cmake_user_inc_examples/cmake.user.inc-singularity.anaconda.gnu.mkl cmake.arch.inc
  mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make VERBOSE=1

  cd /opt/AbInitioToolkit/abinitioLaunchers && python setup.py install

  mkdir -p /dataSingularity
  cp /opt/pyscf/pyscf/nao/notebook/ase/example-ase-siesta-pyscf-h2o.* /dataSingularity/
  chmod 777 -R /dataSingularity
  chmod 777 -R /opt/conda
